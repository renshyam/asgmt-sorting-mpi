#!/bin/bash
#PBS -N myjob
#PBS -q course
#PBS -l nodes=10:ppn=8

cd /home/rengak/asgmt-sorting-mpi
mkdir -p output
for n in 1 2 3 4 5 ; do \
	./quick_sort.out -n 100000 -i data/merge_data.txt \
		-r output/result.txt -t output/time_$n.txt ; \
done
for p in 4 8 16 32 64 80 ; do \
	for n in 1 2 3 4 5 ; do \
		mpiexec -np ${p} -f $PBS_NODEFILE ./parallel_sort.out -n 100000 \
			-i data/merge_data.txt -r output/result_mpi_${p}.txt \
			-t output/time_mpi_${p}_${n}.txt ; \
	done \
done
