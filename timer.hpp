#include <chrono>

class Timer {
   public:
    Timer();
    float GetElapsedTime();
    void reset();

   private:
    std::chrono::steady_clock::time_point start_time;
};
