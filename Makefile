SHELL = /bin/bash
MPICXX = mpicxx
MPIRUN = mpiexec
CXXFLAGS = -std=c++11

all: quick_sort.out parallel_sort.out

quick_sort.out: quick_sort.cpp timer.o
	$(MPICXX) $(CXXFLAGS) -o $@ $^

parallel_sort.out: parallel_sort.cpp timer.o
	$(MPICXX) $(CXXFLAGS) -o $@ $^

timer.o: timer.cpp timer.hpp
	$(MPICXX) $(CXXFLAGS) -c $^

run: quick_sort.out parallel_sort.out
	mkdir -p output
	for n in 1 2 3 4 5 ; do \
		./quick_sort.out -n 100000 -i data/merge_data.txt \
			-r output/result.txt -t output/time_$$n.txt ; \
	done
	for p in 4 8 16 32 64 80 ; do \
	for n in 1 2 3 4 5 ; do \
		$(MPIRUN) -np $${p} ./parallel_sort.out -n 100000 \
			-i data/merge_data.txt -r output/result_mpi_$${p}.txt \
			-t output/time_mpi_$${p}_$${n}.txt ; \
	done \
	done

.PHONY: clean

analyse:
	for p in 4 8 16 32 64 80 ; do \
		for n in 1 2 3 4 5 ; do \
		diff output/result.txt output/result_mpi_$${p}.txt ; \
		done \
	done
	python3 python3/analyse.py

clean:
	rm -f *.gch
	rm -f *.out
	rm -f *.o
	rm -f output/*
